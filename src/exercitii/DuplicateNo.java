package exercitii;

import java.util.Scanner;

public class DuplicateNo {

    //Write a Java program to find the duplicate
    //values of an array of integer values: [1, 7, 3, 7, 10, 1, 9].

    public static void main(String[] args) {
        int[] array;
        int n;

        Scanner sc = new Scanner(System.in);
        System.out.println("Program afisare numere duplicate");


        System.out.print("n= ");  // mesaj citire vector
        n = sc.nextInt();           // citire n de la tastatura
        array = new int[n];         // initializare vector (array)

        for (int i = 0; i < array.length; i++) {
            System.out.print("array[" + i + "]= ");
            array[i] = sc.nextInt();            //parcurgerea vectorului , introducere elemente de la tastatura
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    System.out.print(array[i] + " ");
                }
            }

        }

    }
}

