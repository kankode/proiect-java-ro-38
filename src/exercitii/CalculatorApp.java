package exercitii;


 /*
	Calculator app
	Write a console app that has a menu with 4 basic arithmetic operations: addition, subtraction, multiplication, division
	The app will function as following:
	-Present the menu with operations to the user
	-After the user selects an operation, the program will ask the user to input the operands
	-If the user inputs an invalid option during the operation menu selection, the app will signal the user
with an error message and will ask the user to input a correct option
	-After inputing the operands, the program will show the result and will wait for a user input before returning to
the main operation selection menu
	-The Operation menu will have a dedicated option that will allow the user to exit the program, during the
operations selection menu
  TEMA: Extrageti codul din cazurile meniului in metode separate. In meniu se permite doar apelarea metodei rezultate*/

import java.util.Scanner;

public class CalculatorApp {
    private static double a, b;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int option = 0;

        while (option != 9) {
            displayMenu();
            option = sc.nextInt();
            switch (option) {
                case 1: // Addition
                   _addition(sc);
                    break;
                case 2: // Subtraction
                    _subtraction(sc);
                    break;
                case 3: // Multiplication
                    _multiplication(sc);
                    break;
                case 4: // Division
                    _division(sc);
                    break;
                case 5: //Exit
                    break;
                default: // Wrong selection
                    System.out.println("Wrong option! Please try again!");
                    break;
            }
            System.out.println("Press enter to continue...");
            sc.nextLine(); // Flush scanner buffer
            sc.nextLine(); // Wait for the user to press enter
        }
    }

    public static void readOperands(Scanner sc){
        System.out.print("a=");
        a = sc.nextInt();
        System.out.print("b=");
        b = sc.nextInt();
    }

    public static void displayMenu() {
        System.out.println("Calculator app");
        System.out.println("1.Addition\n" +
                "2.Subtraction\n" +
                "3.Multiplication\n" +
                "4.Division\n" +
                "9.Exit program");
        System.out.print("->");
    }

    public static void _addition(Scanner sc) {
        readOperands(sc);
        System.out.println(a + " + " + b + " = " + (a + b));
    }
    public static void _subtraction(Scanner sc) {
        readOperands(sc);
        System.out.println(a + " - " + b + " = " + (a - b));
    }
    public static void _multiplication(Scanner sc) {
        readOperands(sc);
        System.out.println(a + " * " + b + " = " + (a * b));
    }
    public static void _division(Scanner sc) {
        readOperands(sc);
        System.out.println(a + " / " + b + " = " + (a / b));
    }
}
