package exercitii;

import java.util.Scanner;

public class ReverseArray {
    public static void main(String[] args) {
        int[] myArray;
        int[] reversedArray;
        int n;

        Scanner sc = new Scanner(System.in);

        System.out.print("Input array length: ");

        n = sc.nextInt();

        myArray = new int[n];
        reversedArray = new int[n];

        for(int i = 0; i < myArray.length; i++){
            System.out.print("myArray[" + i + "] = ");
            myArray[i] = sc.nextInt(); // myArray[5] = {1, 5, 4, 3, 8};
            reversedArray[n - i -1] = myArray[i];   // reversedArray[5] = {8, 3, 4, 5, 1};
        }

        System.out.print("Initial Array is: ");
        for(int i = 0; i < myArray.length; i++){
            System.out.print(myArray[i] + " ");
        }
        //System.out.println(" ");
        System.out.print("\nReversed Array is: ");
        for(int i = 0; i < reversedArray.length; i++){
            System.out.print(reversedArray[i] + " ");
        }
    }
}