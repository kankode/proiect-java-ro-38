package exercitii;

import java.util.Scanner;

public class ReverseArray2 {
    public static void main(String[] args) {
        int[] myArray;
        int n;
        Scanner sc = new Scanner(System.in);

        System.out.print("Input array length: ");

        n = sc.nextInt();
        myArray = new int[n];

        for(int i = 0; i < myArray.length; i++){
            System.out.print("myArray[" + i + "] = ");
            myArray[i] = sc.nextInt(); // myArray[5] = {1, 5, 4, 3, 8};
        }

        System.out.print("Initial Array is: ");
        for(int i = 0; i < myArray.length; i++){
            System.out.print(myArray[i] + " ");
        }
        //System.out.println(" ");
        System.out.print("\nReversed Array is: ");
        for(int i = myArray.length - 1; i < myArray.length; i--){
            System.out.print(myArray[i] + " ");
        }
    }
}

