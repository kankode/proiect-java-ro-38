package exercitii;

import java.util.Scanner;

public class MoveSheep1 {
    public static void main(String[] args) {

        boolean[][] myYardArray = new boolean[5][5];

        int i = 0;
        int j = 0;

        Scanner sc = new Scanner(System.in);
        myYardArray[i][j] = true;
        int option = 0;


        while (option != 5) {
            for (int k = 0; k < myYardArray.length; k++) {
                for (int l = 0; l < myYardArray[k].length; l++) {
                    if (myYardArray[k][l] == false) {
                        System.out.print("-  ");
                    } else {
                        System.out.print("#  ");
                    }
                }
                System.out.println("");
            }

            System.out.println("Please select an option: ");
            if (i != 0)
                System.out.println("W. Move up!");
            if (i != myYardArray.length - 1)
                System.out.println("S. Move down!");
            if (j != 0)
                System.out.println("A. Move left!");
            if (j != myYardArray.length - 1)
                System.out.println("D. Move right!");
            System.out.println("E. Exit program");

            option = sc.nextInt();

            switch (option) {
                case 1:
                    myYardArray[i][j] = false;
                    i--;
                    myYardArray[i][j] = true;
                    break;
                case 2:
                    myYardArray[i][j] = false;
                    i++;
                    myYardArray[i][j] = true;
                    break;
                case 3:
                    myYardArray[i][j] = false;
                    j--;
                    myYardArray[i][j] = true;
                    break;
                case 4:
                    myYardArray[i][j] = false;
                    j++;
                    myYardArray[i][j] = true;
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Wrong option");
            }

        }
    }
}
