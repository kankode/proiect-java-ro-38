package exercitii;

import java.util.Scanner;

public class EvenOdd {
    public static void main(String[] args) {
        int[] array;
        int n, oddCounter = 0, evenCounter = 0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduceti lungimea vectorului: ");
        n = sc.nextInt();   // citirea lui n de la tastatura
        array = new int[n];

        for (int i = 0; i < array.length; i++) {  // parcurgerea vectorului / array-ului
            System.out.print("myArray[" + i + "] = ");
            array[i] = sc.nextInt();    //introducerea elementului din vector de la tastatura
            if (array[i] % 2 == 0) {
                evenCounter++; // evenCounter+=1; evenCounter = evenCounter + 1;
            } else {
                oddCounter++;
            }
        }
        System.out.println("Even: " + evenCounter);
        System.out.println("Odd: " + oddCounter);
    }
}