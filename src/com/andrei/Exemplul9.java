package com.andrei;

import java.util.Scanner;

// Media aritmetica

public class Exemplul9 {
    public static void main(String[] args) {
        double[] myArray;
        Scanner sc = new Scanner(System.in);
        double avg = 0;
        int n,i;
        System.out.println("Input no of elements");
        System.out.print("n=");
        n = sc.nextInt();
        myArray = new double[n];
        for(i=0; i<myArray.length; i++) {
            System.out.print("myArray["+i+"]=");
            myArray[i] = sc.nextDouble();
        }
        for(i=0; i<myArray.length; i++) {
            avg = avg + myArray[i];
        }
        avg = avg / myArray.length;
        System.out.println("The average is: " + avg);
    }
}
