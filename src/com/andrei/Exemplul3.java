package com.andrei;

import java.util.Scanner;

public class Exemplul3 {
    public static void main(String[] args) {
        int a, b;
        Scanner scan = new Scanner(System.in);
        System.out.println("Introduceti doua valori de la tastatura");
        System.out.print("a=");
        a = scan.nextInt();   //citeste scanner urmatorul nr intreg din consola
        System.out.print("b=");
        b = scan.nextInt();
        if (b > a) {
            System.out.println("b is greater");
        } else if (b == a) {
            System.out.println("a=b");
        } else {
            System.out.println("a is greater");
        }
    }
}
