package com.andrei;

import java.util.Scanner;

//Write a program which prints "fizz" if the number is a multiplier of 3, prints "buzz" if
//its multiplier of 5 and prints "fizzbuzz" if the number is divisible by both 3 and 5.

public class Exemplul6 {
    public static void main(String[] args) {
        int a;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Problema");
        System.out.print("a=");
        a = scanner.nextInt();

        if (a % 3 == 0)
            System.out.print("fizz");
        if (a % 5 == 0)
            System.out.println("buzz");

}
}
