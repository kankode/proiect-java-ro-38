package com.andrei;

public class Exemplul2 {

    // Exemplificare IF-THEN-ELSE
    public static void main(String[] args) {

        double temperature = 35.5;

        // IF imbricat
        if (temperature > 100) {
            System.out.println("The water is boliling");
        } else {
            if (temperature < 50) {
                System.out.println("The water is getting hotter");
            }
        }
        // ELSE-IF
        if (temperature > 100) {
            System.out.println("The water is boliling");
        } else {
            if (temperature < 50) {
                System.out.println("The water is getting hotter");
            }
        }

    }
}
