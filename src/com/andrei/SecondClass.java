package com.andrei;

public class SecondClass {
    public static void main(String[] args) {
        int y = 5;
        double x = 2 + 2 * --y;
        System.out.println(x);

        // Exemplu operator ternar
        String rezultat;
        rezultat= (5>7)?"Da! Primul nr este mai mare":"Nu! Al doilea numar este mai mare";
        System.out.println(rezultat);
    }
}
