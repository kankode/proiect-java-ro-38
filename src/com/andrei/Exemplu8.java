package com.andrei;

import java.util.Scanner;

public class Exemplu8 {
    public static void main(String[] args) {
        int[][] matrix;
        Scanner sc = new Scanner(System.in);
        int n, m;

        System.out.println("Input number of rows");
        System.out.print("n=");
        n = sc.nextInt();
        System.out.println("Input number of rows");
        System.out.print("m=");
        m = sc.nextInt();
        matrix = new int[n][m];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print("a[" + i + "][" + j + "] = ");
                matrix[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();

        }
    }
}
