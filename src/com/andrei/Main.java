package com.andrei;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a = 0, b = 0;
        int aux;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti doua numere de la tastatura");
        System.out.print("a=");
        a = scanner.nextInt();
        System.out.print("b=");
        b = scanner.nextInt();

        System.out.println("Se inverseaza valorile.....");
        aux = a;
        a = b;
        b = aux;
        System.out.println("a=" + a + " si b=" + b);
    }
}
