package com.andrei;

import java.util.Scanner;

// All odd numbers

public class Exemplul10 {
    public static void main(String[] args) {
        int[] array;
        Scanner scan = new Scanner(System.in);
        int n, i;

        System.out.print("Input the numbers ");
        n = scan.nextInt();
        array = new int[n];

        for (i = 0; i < array.length; i++) {
            System.out.print("myArray[" + i + "]=");
            array[i] = scan.nextInt();
        }
        System.out.print("All odds number: ");

        for (i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i] + " ");

            }
        }
    }
}
