package com.andrei;

import java.util.Scanner;

public class Exemplul5 {
    public static void main(String[] args) {
        int year;
        Scanner scan = new Scanner(System.in);
        System.out.println("Check if leap year");
        System.out.print("year=");
        year = scan.nextInt();

        // Conditions for leap yer
        // year%400 == 0 OR year%4 == 0 AND year%100 != 0

        if ((year % 400 == 0) || (year % 4 == 0) && (year % 100 != 0)){
            System.out.println(year + " is leap year");
        }else{
            System.out.println(year + " is NOT leap year");
        }
    }
}
