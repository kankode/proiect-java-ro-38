package com.andrei;

import java.util.Scanner;

public class Exemplul4 {

    // Verificare numere pare / impare

    public static void main(String[] args) {
        int a;
        Scanner scan = new Scanner(System.in);
        System.out.println("Verificare numar par");
        System.out.print("a=");
        a = scan.nextInt();
        if (a % 2 == 0) {
            System.out.println("a is even");
        } else {
            System.out.println("a is odd");
        }
    }
}
