package com.andrei.extra;

public class Ex1 {
    public static void main(String[] args) {
        int a = 5;
        int b = 4;
        int c = 3;

        int result1 = a - b - c;
        System.out.println("Result1: " + result1);

        long d = 28;
        long e = 35;
        long f = 48;

        long result2 = d * e / f;
        System.out.println("Result2: " + result2);
    }
}
