package com.andrei.extra;

import java.util.Scanner;

public class Ex14 {
    public static void main(String[] args) {

        double r;
        Scanner cerc = new Scanner(System.in);

        System.out.println("Perimeter & Area of Circle");

        System.out.print("Radius = ");
        r = cerc.nextDouble();

        double area = Math.PI * r * r;
        System.out.println("Area = " + area);

        double perimeter = 2 * Math.PI * r;
        System.out.println("Perimeter = " + perimeter);
    }
}
