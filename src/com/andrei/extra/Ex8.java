package com.andrei.extra;

// Implement the program displaying all the number from the range 1-100 witch are
// divisible by 5 beginning from 100 (is reverse order)

public class Ex8 {
    public static void main(String[] args) {
        for(int i=100; i>0; i++)
            if(i%5==0){
                System.out.println(i);
            }
    }
}


