package com.andrei.extra;

import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);

        System.out.print("Input the number: ");
        n = sc.nextInt();

        if (n > 0) {
            System.out.println("Variable is greater");
        } else if (n < 0) {
            System.out.println("Variable is less");
        } else {
            System.out.println("Variable is equal to");
        }
    }
}
