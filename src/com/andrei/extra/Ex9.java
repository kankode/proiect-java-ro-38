package com.andrei.extra;

// Sa se scrie un program care calculeaza suma a 459 de elemente dintr-o secventa care incepe cu elementul 5
// si fiecare element este rezultat din elementul anterior incrementat cu 2 unitati

public class Ex9 {
    public static void main(String[] args) {
        int n = 5;
        int elementeAdunate = 1;
        int sum = 0;

        while (elementeAdunate <= 459) {
            sum += n;
            n = n + 2;
            elementeAdunate++;

        }
        System.out.println(sum);

    }

    public static void varianta2(String[] args) {
        int sum = 0;
        for (int i = 5; i <= 460 * 2 + 1; i = i + 2) {
            sum = sum + i;
        }
        System.out.println("Sum = " + sum);
    }

}