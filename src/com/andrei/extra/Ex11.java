package com.andrei.extra;

import java.util.Scanner;

public class Ex11 {
    public static void main(String[] args) {
        int a;
        int b;
        Scanner sc = new Scanner(System.in);

        System.out.println("PRODUCT OF TWO NUMBERS");

        System.out.print("Input first number: ");
        a = sc.nextInt();
        System.out.print("Input second number: ");
        b = sc.nextInt();

        System.out.println(a + " + " + b + " = " + (a+b));
        System.out.println(a + " - " + b + " = " + (a-b));
        System.out.println(a + " * " + b + " = " + (a*b));
        System.out.println(a + " / " + b + " = " + (a/b));
    }
}
