package com.andrei.extra;

public class Ex4 {
    public static void main(String[] args) {
        int intVar1 = 9;
        int intVar2 = 7;
        short shortSum = (short) (intVar1 + intVar2);
        System.out.println("shortSum: " + shortSum);
        shortSum++;
        System.out.println("shortSum++: " + shortSum++);

        byte byteSum = (byte) shortSum;
        System.out.println("byteSum: " + byteSum);
        System.out.println("++byteSum: " + ++byteSum);


    }
}
