package com.andrei.extra;

import java.util.Scanner;

public class Ex10 {
    public static void main(String[] args) {
        int n = 6;
        double[] array = new double[n];
        Scanner sc = new Scanner(System.in);

        System.out.println("Input the elements: ");

        for(int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            array[i] = sc.nextDouble();
        }
        double avg = 0;
        for(int i=0;i<array.length;i++) {
            avg = avg + array[i];
        }
        avg = avg / n;
        System.out.println(avg);
    }
}
