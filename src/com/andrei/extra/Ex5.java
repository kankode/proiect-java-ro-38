package com.andrei.extra;

/* Please, declare three variables of String type and assign it values.
Then declare a fourth variable of type String which will be a concatenation of previously declared variables and display its value on the screen.
Please do it in two ways:
a) Using ‘+’ operator
b) Using concat() method from String class */



public class Ex5 {
    public static void main(String[] args) {
        String s1 = "1";
        String s2 = "2";
        String s3 = "3";
        String s4 = s1+s2+s3;
        String s5 = s1.concat(s2).concat(s3);

        System.out.println(s4);
        System.out.println(s5);
    }
}
