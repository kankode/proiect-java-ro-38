package com.andrei.extra;

import java.util.Scanner;

public class Ex15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;

        System.out.println("Sum of the digits of an integer");

        System.out.print("Input an integer: ");
        n = sc.nextInt();

        System.out.println("The sum is: " + sumOfDigits(n));
    }

    private static int sumOfDigits(int n) {
        int sum = 0;
        while (n != 0) {
            sum = sum + n % 10;
            n = n / 10;
        }
        return sum;

    }
}
