package com.andrei.recapitulare.array;

public class Exemplu {
    public static void main(String[] args) {

        // Declarare
        int a,n; // Declarare variabila de tip nr intreg
        int[] a1; // Declarare variabila de tip vector de nr intregi
        int a2[],a3; // a2 = vector, a3 - un nr intreg

        // Initializare
        a1 = new int[100]; // Initializare vector cu nr intregi cu 100 elemente
        a2 = new int[]{1,2,3}; // Initializare vector cu 3 elemente

        // Declarare + initializare
        int[] a4 = new int[100];
        int[] a5 = new int[]{1,2,3};

        // Proprietatea length (marimea vectorului)
        System.out.println(a4.length);
        int[] a6;
        // System.out.println(a6.length); nu este posibil deoarece a6 nu este initializat

        // Parcurgere array pentru procesare (citire, scriere, calcule, modificare, etc.)
        // Parcurgerea se efectueaza cu "for"
        // Exemplu: Afisare elemente array in consola
        for(int i=0;i<a5.length;i++){
            System.out.println(a5[i]);
        }

    }
}
