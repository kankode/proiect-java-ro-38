package com.andrei.clasesiobiecte;

public class Farm {
    public static void main(String[] args) {
        Animal c1 = new Animal();
        c1.species = "dog";
        c1.age = 1;
        c1.name = "Rusty";
        c1.domestic = true;
        c1.sound = "Ham ham!";

        System.out.println("Animal details:\n" +
                "Species: " + c1.species +
                "\nName: " + c1.name +
                "\nAge: " + c1.age +
                "\nIs domestic?: " + c1.domestic +
                "\nSound Signature: " + c1.sound);

        //Calling 1st method
        c1.makeSound();

        System.out.println("Dog age: " + c1.age);
        System.out.println("Happy birthday!");
        c1.getOlder();
        System.out.println("Dog age: " + c1.age);
    }
}
