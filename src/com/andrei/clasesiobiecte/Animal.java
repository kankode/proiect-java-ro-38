package com.andrei.clasesiobiecte;

public class Animal {
    //Properties (State)
    String species;
    String name;
    int age;
    boolean domestic;
    String sound;

    //Methods (Behaviour)
    //access-modifier (static) type(ex.void, int, string, etc.) name(parameters)
    //access-modifier (static) type name(parameters)
    public void makeSound() {
        System.out.println(sound);
    }

    public void getOlder() {
        age++;
    }
}
