package com.andrei.clasesiobiecte;

// Un program care citeste de la tastatura un nr de animale, dupa care afiseaza un array cu toate

import java.util.Scanner;

public class Farm2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        Animal[] array;

        System.out.print("Input no of animals: ");
        n = sc.nextInt();
        array = new Animal[n];

        sc.nextLine();

        for (int i = 0; i < array.length; i++) {
            array[i] = new Animal();
            System.out.print("Input the species: ");
            array[i].species = sc.nextLine();
            System.out.print("Input the age: ");
            array[i].age = sc.nextInt();
            sc.nextLine();
            System.out.print("Input the name of animal: ");
            array[i].name = sc.nextLine();
            System.out.print("It's domestic? ");
            array[i].domestic = sc.nextBoolean();
            sc.nextLine();
            System.out.print("Input sound signature: ");
            array[i].sound = sc.nextLine();
        }
        for (int i = 0; i < array.length; i++) {
            System.out.println("Animal details:\n" +
                    "Species: " + array[i].species +
                    "\nName: " + array[i].name +
                    "\nAge: " + array[i].age +
                    "\nIs domestic?: " + array[i].domestic +
                    "\nSound Signature: " + array[i].sound);
        }
    }
}

