package oop;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Student {

    //Declarare camapuri(proprietati specifice clasei)
    public String nume;
    public String prenume;
    public int varsta;
    public LocalDate dataAbsolvirii;
    public double medie;
    public boolean integralist;

    //Declarare constructor fara parametri(conctructor default autogenerat de Java daca nu exista alt constructor)
    public Student() {
    }

    //Declarare constructor cu parametri
    public Student(String nume, String prenume, int varsta, LocalDate dataAbsolvirii, double medie, boolean integralist) {
        this.nume = nume;
        this.prenume = prenume;
        this.varsta = varsta;
        this.dataAbsolvirii = dataAbsolvirii;
        this.medie = medie;
        this.integralist = integralist;
    }

    //Dunctie de afisare pentru clasa Student
    public void afisareStudent() {
        System.out.println("Studentul: nume= " + this.nume + " prenume= " + this.prenume);
        System.out.print(" varsta=" + this.varsta);
        System.out.print(" medie=" + this.medie);
        System.out.print(" dataAbsolvirii=" + this.dataAbsolvirii);
        System.out.print(" integralist=" + this.integralist);
        System.out.println();
    }

    public void inregistrareStudent() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduceti nume: ");
        nume = sc.nextLine();
        System.out.print("Introduceti prenume: ");
        prenume = sc.nextLine();
        System.out.print("Introduceti varsta: ");
        varsta = sc.nextInt();
        System.out.print("Introduceti data absolvirii: ");
        dataAbsolvirii = adaugaData();
        System.out.print("Introduceti media: ");
        medie = sc.nextDouble();
        System.out.println("Integralist ");
        integralist = sc.hasNextBoolean();
    }

    public static LocalDate adaugaData() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduceti data absolvirii: ");
        String dataAbsolvirii = sc.nextLine();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(dataAbsolvirii, dtf);
    }
}
