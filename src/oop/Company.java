package oop;

import java.util.Arrays;

public class Company {
    private String name;
    private Employee[] employees;

    public Company() {

    }

    // Modificatori de acces - void (aceste metode nu returneaza nimic, doar seteaza atribute) - set_nume atribut (tipul obiectului ce va fi setat - nume obiect)
    public void setName(String name) {
        this.name = name;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }

    // Modificatori de acces - tipul de date al obiectului ce va fi returnat - get_nume atribut (aceste metode nu primesc parametri)
    public String getName() {
        return name;
    }
    public Employee[] getEmployees() {
        return this.employees;  // Aceste metode trebuie sa returneze obiectul respectiv
    }

    @Override
    public String toString() {
        return "Company: " +
                "name='" + name + '\'' +
                ", employees=" + Arrays.toString(employees);
    }
}

