package oop;

import java.time.LocalDate;

public class MainCompany {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Irina", "Zaharia", 22, LocalDate.of(1999, 6, 12), 1000);
        Employee employee2 = new Employee("Mihai", "Popescu", 27, LocalDate.of(1995, 4, 19), 1800);
        Employee employee3 = new Employee();

        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println("employee3 inainte de a avea campurile setate");

        System.out.println(employee3);


        employee3.setFirstName("Alex");
        employee3.setLastName("Cornoiu");
        employee3.setAge(31);
        employee3.setDateOfBirth(LocalDate.of(1989, 11, 27));
        employee3.setSalary(2100);

        System.out.println("employee3 dupa setarea campurile");
        System.out.println(employee3);


        Employee[] employees = new Employee[3];
        employees[0] = employee1;
        employees[1] = employee2;
        employees[2] = employee3;


        // Definim un nou obiect de tip companie
        Company company1 = new Company();
        company1.setName("EMAG");
        company1.setEmployees(employees);
        System.out.println(company1);
    }
}
