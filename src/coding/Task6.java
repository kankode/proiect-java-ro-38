package coding;

import java.util.Scanner;

// tratarea cazului cu introducerea numarului 0 (zero)

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("n = ");
        int n = sc.nextInt();

        while(n<=1){
            System.out.print("Wrong number. Insert another number: ");
            n = sc.nextInt();
        }

        double h = 0;

        for (int i = 1; i <= n; i++) {
            h = h + (double) 1 / i;
        }
        System.out.println("Harmonic sum is: " + h);
    }
}


