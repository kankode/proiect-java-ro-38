package coding;

import java.util.Scanner;

public class UglyNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduceti un numar: ");

        int n = sc.nextInt();

        if (n % 2 == 0 && n % 3 == 0 || n % 2 == 0 && n % 5 == 0 || n % 3 == 0 && n % 5 == 0) {
            System.out.println("Is an ugly number");
        }else{
            System.out.println("It's not ugly number");
        }
    }
}
