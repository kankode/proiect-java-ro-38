package coding;

import java.util.Scanner;

public class FizzBuzz {
    public static void main(String[] args) {
        // Introducerea de la tastatura a nr pozitiv
        Scanner sc = new Scanner(System.in);
        System.out.println("Program FizzBuzz");
        System.out.print("a = ");
        int a = sc.nextInt();

        //Parcurgerea pana la nr introdus si interpretarea conditiilor
        for (int i = 1; i <= a; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }
}

