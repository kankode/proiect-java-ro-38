package coding;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = "";
        int maxLength = 0;
        String longText = "";

        while (!text.equals("stop")) {
            System.out.print("Introduceti textul: ");
            text = sc.nextLine();
            if (maxLength < text.length()) {
                maxLength = text.length();
                longText = text;
            }
        }
        System.out.println(longText);

    }
}
