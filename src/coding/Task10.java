package coding;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n;
        int m;
        int sum = 0;

        System.out.print("n = ");
        n = sc.nextInt();

        while (n > 0) {
            m = n % 10;
            sum = sum + m;
            n = n / 10;
        }
        System.out.println("Sum = " + sum);
    }
}
