package coding;

import java.util.Scanner;

public class Tema2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Media aritmetica a elementelor unui vector");
        System.out.print("n= ");
        int n = sc.nextInt();
        int[] vector = new int[n];
        double avg = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("vector[" + i + "]= ");
            vector[i] = sc.nextInt();
        }

        for (int i = 0; i < n; i++) {
            avg = (avg + vector[i]);
        }
        System.out.println("Media aritmetica este: " + avg / n);
    }
}

