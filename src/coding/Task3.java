package coding;

import java.util.Scanner;

// Ecuatie gradul II

public class Task3 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Introduceti primul coeficient a: ");
        int a = sc.nextInt();

        System.out.print("Introduceti al doilea coeficient b: ");
        int b = sc.nextInt();

        System.out.print("Introduceti al treilea coeficient c: ");
        int c = sc.nextInt();


        // double delta = Math.pow(b,2)-4*a*c;

        // int delta = (int)Math.pow(b,2)-4*a*c;

        int delta = b * b - 4 * a * c;

        if(delta<0) {
            System.out.println("Delta este negativ");
            // return;
            System.exit(0);  // iesire cu succes din program
        }

        // Calculam solutiile ecuatiei de gradul 2
        double x1 = (-b - Math.sqrt(delta))/2*a;
        double x2 = (-b + Math.sqrt(delta))/2*a;

        System.out.println("Solutiile ecuatiei sunt: ");
        System.out.println(x1);
        System.out.println(x2);
        }


}
